#!/usr/bin/python3

########################################################
# Program that acts like a jukebox                     #
# It uses:                                             #
# - A HC-SR04 ultrasonic sensor for presence detection #
# - 1 push button to play another song                 #
# - 1 push button to change the current language of the songs #
########################################################

import subprocess
import time
import logging
import signal
import RPi.GPIO as GPIO
import sys
import configparser
import random
from os import listdir
from os.path import isfile, join

CONFIG_FILE = 'config.ini'

DEFAUT_LOG_LEVEL = logging.INFO

my_config = {
    'presenceDetectDelay': 0.2,    # Delay between 2 ultrasonic detections
    'presenceDetectNb': 2,      # How many sensor reading to detect a presence
    'presenceDetectAt': 25,     # cm
    'buttonPressedDelay': 0.2,
    'buttonNextSongRestartDelay': 0.5,
    'buttonLangPin': 3,
    'buttonNextSongPin': 5,
    # Ultrasonic sensor
    'trigPin': 16,
    'echoPin': 18,
}

SONG_DIRS = ['../resources/it', '../resources/en']
ANNOUNCEMENTS = ['../resources/lang_it.mp3', '../resources/lang_en.mp3']

# When a button is pressed we get value 0
BUTTON_PRESSED = 0

media = {
    'song': {
        'process': None,
        'source': [None]
    },
    'announcements': {
        'process': None,
        'source': ANNOUNCEMENTS
    },
}

song_dir_list = SONG_DIRS
song_files = []

cur_lang = 0
presence_check_time = 0
btn_next_song_pressed_time = 0
presence = []
presence_detected = False
ps_cur_song = None # process of the current song

def is_media_playing(media_type):
    ps = media[media_type]['process']
    if ps == None:
        return False
    return ps.poll() == None

def play_media(media_type, num_media=0):
    global media
    logging.info("Playing media: %s" % media[media_type]['source'][num_media])
    ps = subprocess.Popen(['omxplayer', media[media_type]['source'][num_media]],
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE, 
                close_fds=True
    ) 
    media[media_type]['process'] = ps

def pickup_random_song(lang):
    global media
    # Pick up random file among the list
    # logging.debug("Random %s" % random.randrange(len(song_files[lang])))
    song_number = random.randrange(len(song_files[lang]))
    media['song']['source'][0] = song_files[lang][song_number]

def play_random_song(lang):
    pickup_random_song(lang)
    play_media('song')

def stop_media(media_type):
    global media
    if is_media_playing(media_type):
        media[media_type]['process'].stdin.write(bytes('q', 'UTF-8')) # stdin.write('q') in Python2
        media[media_type]['process'] = None

def get_presence_dist():
    logging.debug("Starting ultrasonic sensor")
    # Start triggering
    for i in range(0, my_config['presenceDetectNb']):
        GPIO.output(my_config['trigPin'], True)
        time.sleep(0.00001)
        GPIO.output(my_config['trigPin'], False)

        pulse_start = 0
        pulse_end = time.time()
        pulse_duration = 0
        measuring_start = time.time()

        # Measuring echo duration: time elapsed from high state to low state of echo pin
        while GPIO.input(my_config['echoPin']) == 0: # wait until echo pin is high (1)
            pulse_start = time.time()
            if time.time() - measuring_start > 1:
                pulse_start = 0
                logging.warning("Echo timeout")
                break
          
        if pulse_start != 0:
            while GPIO.input(my_config['echoPin']) == 1: # wait until echo pin is low (0)
                pulse_end = time.time()
            pulse_duration = pulse_end - pulse_start

        # Calculate the distance (speed of the sound in the air: 343 m/s => 34300 cm/s)
        ## distance = speed * time
        ## distance = 34300 * time
        # As the echo gives the time it takes to travel to the object and back again
        # we need to divide it by two
        ## distance = 17150 * time
        distance = 17150 * pulse_duration
        distance = round(distance, 2)
        logging.debug("Ultrasonic distance: %s" % distance)
        if not is_presence(distance):
            logging.debug("Distance non OK")
            break
        time.sleep(my_config['presenceDetectDelay'])
    else:
        # All distances in the loop are OK
        return distance 
    return -1

def is_presence(distance):
    return distance > 0 and distance <= my_config['presenceDetectAt']

def gpio_setup():
    GPIO.setmode(GPIO.BOARD) # Use board pin numbering

    GPIO.setup(my_config['buttonLangPin'], GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(my_config['buttonNextSongPin'], GPIO.IN, pull_up_down=GPIO.PUD_UP)

    GPIO.setup(my_config['trigPin'], GPIO.OUT) # trigger => output
    GPIO.setup(my_config['echoPin'], GPIO.IN)  # echo => input
    GPIO.add_event_detect(my_config['echoPin'], GPIO.BOTH)
    
    GPIO.output(my_config['trigPin'], False) # Ensure that the Trigger pin is set low
    
def read_config_file():
    global media, my_config, nb_lang, song_dir_list
    config = configparser.ConfigParser()
    if len(config.read(CONFIG_FILE)) == 0:
        logging.info("No config file found, using default values")
        logging.debug(my_config)
        return
    logging.info("Reading config file")
    song_dir_list = [config['songs_dirs'][f] for f in config['songs_dirs']]
    media['announcements']['source'] = [config['announcements'][f] for f in config['announcements']]
    nb_lang = len(media['announcements']['source'])
    logging.info("Nb of languages: %s" % nb_lang)

    my_config['presenceDetectNb'] = int(config['general'].get('presenceDetectNb'))
    my_config['presenceDetectDelay'] = float(config['general'].get('presenceDetectDelay'))
    my_config['presenceDetectAt'] = int(config['general'].get('presenceDetectAt'))

    my_config['buttonPressedDelay'] = float(config['buttons'].get('buttonPressedDelay'))
    my_config['buttonNextSongRestartDelay'] = float(config['buttons'].get('buttonNextSongRestartDelay'))

    my_config['buttonLangPin'] = int(config['pins'].get('buttonLangPin'))
    my_config['buttonNextSongPin'] = int(config['pins'].get('buttonNextSongPin'))
    my_config['trigPin'] = int(config['pins'].get('trigPin'))
    my_config['echoPin'] = int(config['pins'].get('echoPin'))
    logging.debug(my_config)
    
def signal_ctrl_c(signal, frame):
    logging.info("Ctrl+C pressed!")
    GPIO.cleanup()
    sys.exit(0)

def print_usage():
    print("Usage: %s [log_level] [path_to_log_file]" % sys.argv[0])

if __name__ == "__main__":
    log_file = None
    log_level = DEFAUT_LOG_LEVEL

    if len(sys.argv) > 1:
        level = sys.argv[1].upper()
        log_level = getattr(logging, level)
        if len(sys.argv) > 2:
            log_file = sys.argv[2]

    logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=log_level, filename=log_file)

    signal.signal(signal.SIGINT, signal_ctrl_c)
    
    read_config_file()

    # Load song file names
    for song_dir in song_dir_list:
        song_files.append([ join(song_dir, f) for f in listdir(song_dir) if isfile(join(song_dir,f)) ])

    logging.debug(song_files)
    
    gpio_setup()

    logging.info("Ultrasonic sensor warmup...")
    for i in range(0, 10):
        get_presence_dist()
        time.sleep(1)
    logging.info("Done, starting the app!")

    while True:
        btn_lang = GPIO.input(my_config['buttonLangPin'])
        if btn_lang == BUTTON_PRESSED:
            logging.debug("Button language pressed")
            stop_media('song')
            if cur_lang < nb_lang -1:
                cur_lang = cur_lang + 1
            else:
                cur_lang = 0
            time.sleep(0.2)
            logging.debug("Cur lang : %s" % cur_lang)
            
            play_media('announcements', cur_lang)
            while is_media_playing('announcements'):
                pass
            play_random_song(cur_lang)
            time.sleep(my_config['buttonPressedDelay'])

        btn_audio = GPIO.input(my_config['buttonNextSongPin'])
        if btn_audio == BUTTON_PRESSED:
            logging.debug("Button next song pressed")
            if (time.time() - btn_next_song_pressed_time) > my_config['buttonNextSongRestartDelay']:
                btn_next_song_pressed_time = time.time()
                logging.debug("Playing next song")
                stop_media('song')
                play_random_song(cur_lang)
            time.sleep(my_config['buttonPressedDelay'])
        
        if not is_media_playing('song'):
            if is_presence(get_presence_dist()):
                logging.debug("Presence OK")
                play_random_song(cur_lang)
            else:
                logging.debug("No presence detected")
